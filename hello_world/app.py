import json
from flask_lambda import FlaskLambda
import boto3
from flask import request

app = FlaskLambda(__name__)
database = boto3.resource("dynamodb")
table = database.Table("hello")


@app.route("/hello")
def index():
    data = {
        "message": "hello world",
    }
    return (
        json.dumps(data),
        200,
        {'Content-Type': 'application/json'}
    )


@app.route("/list", methods=["GET", "POST"])
def list_hello():
    if request.method == "GET":
        items = table.scan()["Items"]
        return (
            json.dumps(items),
            200,
            {'Content-Type': 'application/json'}
        )
